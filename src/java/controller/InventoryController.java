
package controller;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.ModelAndView;
import service.ProductManager;

/**
 *
 * @author Elvis
 */
public class InventoryController implements Controller {
    
    protected final Log logger = LogFactory.getLog(getClass());
    private ProductManager productManager;
    
    @Override
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String now = (new Date()).toString();
        Map<String, Object> myModel = new HashMap<String, Object>();
        logger.info("Returning hello view " + now);
        myModel.put("products", this.productManager.getProducts());
        myModel.put("now", now);
        return new ModelAndView("hello", "model", myModel);
    }
    
    public void setProductManager(ProductManager pM) {
        this.productManager = pM;
    }
}
