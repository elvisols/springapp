
package service;

import domain.Product;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Elvis
 */
public interface ProductManager extends Serializable {
    
    public void increasePrice(int percentage);
    
    public List<Product> getProducts();
}
