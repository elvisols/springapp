
package service;

import domain.Product;
import java.util.List;
import model.ProductDao;

/**
 *
 * @author Elvis
 */
public class SimpleProductManager implements ProductManager {

//    private List<Product> products;
    private ProductDao productDao; //new
    
    @Override
    public List<Product> getProducts() {
//        return products;
        return productDao.getProductList(); //new
    }
    
    @Override
    public void increasePrice(int percentage) {
        List<Product> products = productDao.getProductList(); //new
        if(products != null) {
            for(Product product : products) {
                double newPrice = product.getPrice().doubleValue() * (100 + percentage) / 100;
                product.setPrice(newPrice);
                productDao.saveProduct(product); //new
            }
        }
    }
    
    public void setProductDao(ProductDao pdao) {
        this.productDao = pdao;
    }
//    public void setProducts(List<Product> products) {
//        this.products = products;
//    }
}
