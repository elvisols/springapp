
package model;

import domain.Product;
import java.util.List;

/**
 *
 * @author Elvis
 */
public interface ProductDao {
    
    public List<Product> getProductList();
    
    public void saveProduct(Product prod);

}
