/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controller;

import domain.Product;
import java.util.ArrayList;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import junit.framework.TestCase;
import model.InMemoryProductDao;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
//import static org.junit.Assert.*;
import org.springframework.web.servlet.ModelAndView;
import service.SimpleProductManager;

/**
 *
 * @author Elvis
 */
public class InventoryControllerTest extends TestCase{
    
    public InventoryControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of handleRequest method, of class InventoryController.
     */
    @Test
    public void testHandleRequest() throws Exception {
        System.out.println("handleRequest");
        HttpServletRequest request = null;
        HttpServletResponse response = null;
        InventoryController controller = new InventoryController();
        SimpleProductManager spm = new SimpleProductManager(); //new
        spm.setProductDao(new InMemoryProductDao(new ArrayList<Product>())); //new
        controller.setProductManager(spm); //new
//        controller.setProductManager(new SimpleProductManager());
        //ModelAndView expResult = null;
        ModelAndView mv = controller.handleRequest(request, response);
        assertEquals("hello", mv.getViewName());
        assertNotNull(mv.getModel());
        Map modelMap = (Map) mv.getModel().get("model");
        String nowValue = (String) modelMap.get("now");
//        String nowValue = (String) mv.getModel().get("now");
        assertNotNull(nowValue);
        //assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
}
