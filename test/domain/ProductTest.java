/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package domain;

import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

/**
 *
 * @author Elvis
 */
public class ProductTest {
    
    private Product product;
    
    @Before
    public void setUp() throws Exception {
        this.product = new Product();
    }

    /**
     * Test of getDescription and setDescription method, of class Product.
     */
    @Test
    public void testSetAndGetDescription() {
        System.out.println("Testing SetAndGetDescription");
        String testDescription = "aDescription";
        assertNull(this.product.getDescription());
        this.product.setDescription(testDescription);
        assertEquals(testDescription, this.product.getDescription());
    }

    /**
     * Test of setPrice and getPrice method, of class Product.
     */
    @Test
    public void testSetAndGetPrice() {
        System.out.println("Testing setPrice and getPrice");
        double testPrice = 100.10;
        assertEquals(0, 0, 0);
        assertNull(this.product.getPrice());
        this.product.setPrice(testPrice);
        assertEquals(testPrice, this.product.getPrice(), 0);
    }

}
