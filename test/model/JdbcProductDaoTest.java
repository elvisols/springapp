/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

import domain.Product;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.test.AbstractTransactionalDataSourceSpringContextTests;

/**
 *
 * @author Elvis
 */
public class JdbcProductDaoTest extends AbstractTransactionalDataSourceSpringContextTests {
    
    private ProductDao productDao;
    
    public void setProductDao(ProductDao productDao) {
        this.productDao = productDao;
    }

    @Override
    protected String[] getConfigLocations() {
        return new String[]{"classpath:test-context.xml"};
    }

    @Override
    protected void onSetUpInTransaction() throws Exception {
        super.deleteFromTables(new String[]{"products"});
        super.executeSqlScript("file:db/load_data.sql", true);
    }
    
    /**
     * Test of getProductList method, of class JdbcProductDao.
     */
    @Test
    public void testGetProductList() {
        List<Product> products = productDao.getProductList();
        assertEquals("wrong number of products?", 3, products.size());
    }

    /**
     * Test of saveProduct method, of class JdbcProductDao.
     */
    @Test
    public void testSaveProduct() {
        List<Product> products = productDao.getProductList();
        for(Product p : products) {
            p.setPrice(200.12);
            productDao.saveProduct(p);
        }
        List<Product> updatedProducts = productDao.getProductList();
        for(Product p : updatedProducts) {
            assertEquals("wrong price of product?", 200.12, p.getPrice());
        }
    }
    
}
